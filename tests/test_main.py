import functools
import os
from typing import List

import pydicom
from main import generate_traceability_string, main

def assert_tags(ds, de, traceability_text: str, tag):
        if de.keyword in tag:
            assert traceability_text in de.value

def test_main(tmpdir):
    config = {
         "ROIDescription": {
              "overwrite": True,
              "values": ["RAW:HEST", "ENV:PATH"],
              "delimiter": "_"
         }
    }
    main(
        src_dir=os.path.join(os.path.dirname(__file__), "test_data"),
        dst_dir=tmpdir,
        config=config
    )
    for tag, d in config.items():
        trace_text = generate_traceability_string(**d)     
        cb = functools.partial(assert_tags,
                            traceability_text=trace_text,
                            tag=tag)
        
        for fol, subs, files in os.walk(tmpdir):
            for file in files:
                if file == "rtstruct_predictions.dcm":
                    ds = pydicom.dcmread(os.path.join(fol, file))
                    ds.walk(cb)
