# Dicom Tag Writer
This container updates dicom tags according to /config/config.yaml

 
## Defaults
```
ENV CONFIG_PATH=/config/config.yaml
ENV INPUT=/input
ENV OUTPUT=/output
```

`cat /config/config.yaml`
```
ROIDescription: # Keys are DICOM tags to update
  values: ["RAW:TraceabilityTAG", "ENV:PATH"] # Values to generate traceability text from.
                                              # Can start with "RAW:" or "ENV:". 
                                              # "RAW:" will be treated directly as is after the ":"
                                              # "ENV:" will load the environment variable with this name.
  delimiter: "_"  # values will be joined with this value. Default is "_"
  overwrite: True # If the DICOM tag should be overwritten. If False, then traceability string will be appended.
                  # Default is True.
```

## RadDeploy Example
```
models:
  - docker_kwargs:
      image: registry.gitlab.com/dcpt-clinical/dicomflow/operators/dicom_traceability:latest
    input_mounts:
      src: /input
    output_mounts:
      dst: /output
    config:
      ROIDescription: # ROIDescription = SomeModelV3_5.2.26(1)-release
        values: ["RAW:SomeModelV3", "ENV:BASH_VERSION"]
        delimiter: "_"
        overwrite: True
      SeriesDescription: # SeriesDescription = ExistingSeriesDescription++LookMom!
        values: ["RAW:LookMom!"]
        overwrite: False
        delimiter: "++"
```