FROM python:3.12-slim

ADD requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

ENV INPUT=/input
ENV OUTPUT=/output

ENV APP_BASE_DIR=/opt/app/
ENV CONFIG_PATH=/config/config.yaml
ENV FLOW_PATH=/flow/flow.yaml

RUN mkdir -p ${INPUT} ${OUTPUT}

WORKDIR ${APP_BASE_DIR}
COPY . .
ENTRYPOINT ["/bin/sh", "-c"]
CMD ["python ./main.py"]