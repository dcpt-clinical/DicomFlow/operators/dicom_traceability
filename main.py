import functools
import json
import os
import shutil
import sys
from typing import List

import pydicom
import yaml

def add_traceability(ds, de, tag: str, traceability_text: str, overwrite: bool):
    if de.keyword == tag:
        if overwrite:
            de.value = traceability_text
        else:
            de.value = de.value + traceability_text
        print(f"Wrote TO ELEMENT: {de}")

def update_all_dicom_files_in(directory, tag, traceability_string, overwrite: bool = True, **kwargs):
    cb = functools.partial(add_traceability,
                           traceability_text=traceability_string,
                           tag=tag,
                           overwrite=overwrite)
    
    for fol, subs, files in os.walk(directory):
        for file in files:
            p = os.path.join(fol, file)
            try:
                ds = pydicom.dcmread(p, force=True)
                ds.walk(cb, recursive=True)
                print(f"Saving dataset to {p}")
                ds.save_as(p)
            except Exception as e:
                print(e)

def generate_traceability_string(values: List[str], delimiter: str = "_", overwrite: bool = True):
        traceability_values = []
        if overwrite:
            traceability_values.append(delimiter)
            
        for val in values:
            if val.startswith("ENV:"):
                traceability_values.append(os.environ.get(val[4:]))
            elif val.startswith("RAW:"):
                traceability_values.append(val[4:])
            else:
                raise Exception("Unknown value prefix. Must be either 'RAW:' or 'ENV:'.")
        return delimiter.join(traceability_values)

def main(src_dir: str,
         dst_dir: str,
         config: dict) -> None:
    shutil.copytree(src_dir, dst_dir, dirs_exist_ok=True)
    for tag, d in config.items():
        traceability_string = generate_traceability_string(**d)
        update_all_dicom_files_in(dst_dir, tag, traceability_string, **d)
 


if __name__ == "__main__":
    with open(os.environ["CONFIG_PATH"]) as r:
        config = yaml.safe_load(r)
    print(config)
    src = os.environ["INPUT"]
    dst = os.environ["OUTPUT"]
    main(src_dir=src, dst_dir=dst, config=config)
